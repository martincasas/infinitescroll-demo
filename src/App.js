import "./styles.css";
import { useGetPokemonByNameQuery } from "./services/pokemonSlice";
import InfiniteScrollRTK from "./components/InfiniteScroll";

export default function App() {
  const renderItem = (item, onDiscardItem) => {
    return (
      <div
        style={{
          height: "50px", 
          background: 'lightblue',
          marginTop: '16px',
          width: "200px",
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        {item?.name}
      </div>
    );
  };

  const onDiscardItem = (itemUid) => {};

  return (
    <div className="App">
      <div style={{
        display: 'flex',
        justifyContent: 'center',
        border: '2px solid red',
        overflow: 'auto',
        height: '50vh'
      }}>
        <InfiniteScrollRTK
          useQuery={useGetPokemonByNameQuery}
          additionalQueryParams={{ }}
          renderEmpty={() => <></>}
          renderCard={({ item }) => renderItem(item, onDiscardItem)}
          pageSize={5}
        />
      </div>
    </div>
  );
}
