import { Box, CircularProgress } from "@mui/material";
import React, { useEffect, useRef, useState } from "react";

export function BidirectionalFlatList(props) {
  const {
    data,
    hasMoreTop,
    hasMoreBottom,
    isLoading,
    onStartReachedThreshold = 0,
    onEndReachedThreshold = 0,
    onEndReached,
    onStartReached,
    renderItem,
    renderListEmptyComponent,
    renderListHeaderComponent,
    renderListFooterComponent
  } = props;
  const divRef = useRef(null);

  const [scrollPosition, setScrollPosition] = useState("middle");

  const handleScroll = (event) => {
    const { target } = event;
    if (
      target.scrollHeight - target.scrollTop <=
      target.clientHeight + onEndReachedThreshold &&
      hasMoreBottom &&
      !isLoading
    ) {
   
      setScrollPosition("bottom");
      onEndReached();
    } else {
      setScrollPosition("middle");
    }
    if (
      target.scrollTop <= onStartReachedThreshold &&
      hasMoreTop &&
      !isLoading
    ) {
      setScrollPosition("top");
      onStartReached();
    } else {
      setScrollPosition("middle");
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [hasMoreBottom, hasMoreTop, isLoading]);

  return (
    <>
      {renderListHeaderComponent
        ? renderListHeaderComponent(scrollPosition === "top")
        : null}
      <div
        style={{
          /* this padding adds an extra space to allow scrolling
            in case the page fits exactly the screen */
          paddingBottom: 25,
          flexDirection: "column",
          overflow: "auto",
        }}
        onScroll={handleScroll}
        ref={divRef}
      >
        {data?.map((item) => renderItem({ item }))}
        {data?.length === 0 && !isLoading ? renderListEmptyComponent() : null}
        {isLoading ? (
          <Box
            sx={{ width: "100%", display: "flex", justifyContent: "center" }}
          >
            <CircularProgress />
          </Box>
        ) : null}
      </div>
      {renderListFooterComponent
        ? renderListFooterComponent(scrollPosition === "bottom")
        : null}
    </>
  );
}
