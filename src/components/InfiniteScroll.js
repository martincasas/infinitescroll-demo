import React, { useMemo, useState } from "react";
import { BidirectionalFlatList } from "./BidirectionalList";
import { CircularProgress } from "@mui/material";

const DEFAULT_PAGE_SIZE = 10;

function InfiniteScrollRTK(props) {
  const {
    renderEmpty,
    renderCard,
    useQuery,
    additionalQueryParams,
    pageSize = DEFAULT_PAGE_SIZE
  } = props;

  const [page, setPage] = useState(1);

  // skip only avoids first load
  const previousResult = useQuery(
    { ...additionalQueryParams, pageNumber: page - 1, pageSize },
    { skip: page === 1 }
  );

  const currentResult = useQuery({
    ...additionalQueryParams,
    pageNumber: page,
    pageSize
  });
  const nextResult = useQuery({
    ...additionalQueryParams,
    pageNumber: page + 1,
    pageSize
  });

  const isLastPage = false;

  const isFetching =
    currentResult.isFetching ||
    nextResult.isFetching ||
    previousResult.isFetching;
  const firstLoad =
    currentResult.isLoading || nextResult.isLoading || previousResult.isLoading;

  const items = useMemo(() => {
    const array = [];
    // when scrolling up, the previous data may exist, even if the current page is 0
    if (previousResult.data && page !== 0)
      array.push(...previousResult.data);
    if (currentResult.data) array.push(...currentResult.data);
    // the next data may exist in the cache, even if the current page is the last one
    if (nextResult.data && !isLastPage) array.push(...nextResult.data);
    return array;
  }, [
    previousResult.data,
    currentResult.data,
    nextResult.data,
    page,
    isLastPage
  ]);

  const changePage = async (goBack) => {
 
    const allowPageChange = !isFetching && (!isLastPage || !!goBack);

    if (!allowPageChange) return;

    const direction = goBack ? -1 : 1;
    const nextPage = page + direction;

    if (nextPage < 0) return;
    setPage(nextPage);
  };

  if (firstLoad)
    return (
      <div>
        <CircularProgress />
      </div>
    );

  const renderFooter = (isFetching, show) =>
    show ? <div>{isFetching && <CircularProgress />}</div> : null;

  return (
    <BidirectionalFlatList
      data={items}
      renderItem={renderCard}
      renderListEmptyComponent={renderEmpty}
      renderListFooterComponent={(show) => renderFooter(isFetching, show)}
      renderListHeaderComponent={(show) => renderFooter(isFetching, show)}
      onEndReached={async () => changePage()}
      onStartReached={async () => changePage(true)}
      hasMoreTop={
        page > 1
      }
      hasMoreBottom={true}
      isLoading={isFetching || firstLoad}
      // onEndReachedThreshold={300}
      // onStartReachedThreshold={300}
    />
  );
}

export default InfiniteScrollRTK;
