import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

// Define a service using a base URL and expected endpoints
export const pokemonApi = createApi({
  reducerPath: "pokemonApi",
  baseQuery: fetchBaseQuery({ baseUrl: "https://api.punkapi.com/v2" }),
  endpoints: (builder) => ({
    getPokemonByName: builder.query({
      query: ({ pageNumber, pageSize}) =>  ({
        url: '/beers',
        method: 'GET',
        params: { page: pageNumber, per_page: pageSize}
      }),
    })
  })
});

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { useGetPokemonByNameQuery } = pokemonApi;
